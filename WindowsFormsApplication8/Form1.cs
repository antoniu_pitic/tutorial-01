﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication8
{
    public partial class Form1 : Form
    {
        // Reset all the controls to the user's default Control color.  
        private void ResetAllControlsBackColor(Control control)
        {
            control.BackColor = SystemColors.Control;
            control.ForeColor = SystemColors.ControlText;
            foreach (Control childControl in control.Controls)
            {
                ResetAllControlsBackColor(childControl);
            }
        }


        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ResetAllControlsBackColor(this);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // Show the color dialog.
            DialogResult result = colorDialog1.ShowDialog();
            // See if user pressed ok.
            if (result == DialogResult.OK)
            {
                Color color = colorDialog1.Color;
                ResetAllControlsBackColor(this,color);
            }
        }

        private void ResetAllControlsBackColor(Control control, Color color)
        {
            control.ForeColor = color;
            foreach (Control childControl in control.Controls)
            {
                ResetAllControlsBackColor(childControl,color);
            }
        }
    }
}
